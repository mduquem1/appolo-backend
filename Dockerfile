FROM python:3.9.6
ENV PYTHONUNBUFFERED=1
WORKDIR /app
COPY . .
COPY requirements.txt requirements.txt
RUN apt-get update
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN pip install gunicorn
EXPOSE 3000
