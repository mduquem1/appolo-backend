from typing import Dict

from decouple import config
from flask import Flask

def create_app(config_dict: Dict = {}) -> Flask:
    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://{}:{}@{}:{}/{}".format(
        config("DB_USERNAME"),
        config("DB_PASSWORD"),
        config("DB_HOST"),
        config("DB_PORT"),
        config("DB_NAME"),
    )
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config["isolation_level"] = "READ COMMITTED"
    app.config["JWT_SECRET_KEY"] = "frase-ultra-secreta"
    app.config["PROPAGATE_EXCEPTIONS"] = True
    app.config["TESTING"] = config_dict.get("TESTING", False)
    return app