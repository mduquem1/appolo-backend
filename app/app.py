from flask_cors import CORS
from app import create_app
from .models import db
from .routes import create_api

app = create_app()
app_context = app.app_context()
app_context.push()

db.init_app(app)
# db.create_all()

cors = CORS(app)

create_api(app)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)
