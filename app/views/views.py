from app.models import User, db
from flask_restful import Resource
from flask import request
# from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required
from sqlalchemy import exc


class HealthCheck(Resource):
    def get(self):
        return {"status": "ok", "message": "API up and running"}


class Stores(Resource):
    def get(self):
        return [
            {
                "id": 1,
                "name": "Eco Canes",
                "description": "La mejor guarderia",
                "registeredSince": "20/07/2021",
                "photos": [
                    "https://www.patasarriba.cl/wp-content/uploads/2018/05/patasarribanota-nota6.jpg"
                ],
                "email": "ecocanes@email.com",
                "address": "Calle 123 #4 -45",
                "city": "La Calera",
                "department": "Cundinamarca",
                "phone": "+573213232121"
            },
            {
                "id": 2,
                "name": "Eco Canes 2",
                "description": "La mejor guarderia",
                "registeredSince": "20/07/2021",
                "photos": [
                    "https://www.patasarriba.cl/wp-content/uploads/2018/05/patasarribanota-nota6.jpg"
                ],
                "email": "ecocanes2@email.com",
                "address": "Calle 123 #4 -45",
                "city": "La Calera",
                "department": "Cundinamarca",
                "phone": "+573213232121"
            },
            {
                "id": 3,
                "name": "Eco Canes 2",
                "description": "La mejor guarderia",
                "registeredSince": "20/07/2021",
                "photos": [
                    "https://www.patasarriba.cl/wp-content/uploads/2018/05/patasarribanota-nota6.jpg"
                ],
                "email": "ecocanes2@email.com",
                "address": "Calle 123 #4 -45",
                "city": "La Calera",
                "department": "Cundinamarca",
                "phone": "+573213232121"
            },
            {
                "id": 4,
                "name": "Eco Canes 2",
                "description": "La mejor guarderia",
                "registeredSince": "20/07/2021",
                "photos": [
                    "https://www.patasarriba.cl/wp-content/uploads/2018/05/patasarribanota-nota6.jpg"
                ],
                "email": "ecocanes2@email.com",
                "address": "Calle 123 #4 -45",
                "city": "La Calera",
                "department": "Cundinamarca",
                "phone": "+573213232121"
            },
            {
                "id": 5,
                "name": "Eco Canes 2",
                "description": "La mejor guarderia",
                "registeredSince": "20/07/2021",
                "photos": [
                    "https://www.patasarriba.cl/wp-content/uploads/2018/05/patasarribanota-nota6.jpg"
                ],
                "email": "ecocanes2@email.com",
                "address": "Calle 123 #4 -45",
                "city": "La Calera",
                "department": "Cundinamarca",
                "phone": "+573213232121"
            },
            {
                "id": 6,
                "name": "Eco Canes 2",
                "description": "La mejor guarderia",
                "registeredSince": "20/07/2021",
                "photos": [
                    "https://www.patasarriba.cl/wp-content/uploads/2018/05/patasarribanota-nota6.jpg"
                ],
                "email": "ecocanes2@email.com",
                "address": "Calle 123 #4 -45",
                "city": "La Calera",
                "department": "Cundinamarca",
                "phone": "+573213232121"
            },
            {
                "id": 7,
                "name": "Eco Canes 2",
                "description": "La mejor guarderia",
                "registeredSince": "20/07/2021",
                "photos": [
                    "https://www.patasarriba.cl/wp-content/uploads/2018/05/patasarribanota-nota6.jpg"
                ],
                "email": "ecocanes2@email.com",
                "address": "Calle 123 #4 -45",
                "city": "La Calera",
                "department": "Cundinamarca",
                "phone": "+573213232121"
            },
            {
                "id": 8,
                "name": "Eco Canes 2",
                "description": "La mejor guarderia",
                "registeredSince": "20/07/2021",
                "photos": [
                    "https://www.patasarriba.cl/wp-content/uploads/2018/05/patasarribanota-nota6.jpg"
                ],
                "email": "ecocanes2@email.com",
                "address": "Calle 123 #4 -45",
                "city": "La Calera",
                "department": "Cundinamarca",
                "phone": "+573213232121"
            }
        ]

class SingUpView(Resource):
    def post(self):
        if request.json["password1"] != request.json["password2"]:
            return {"mensaje": "password 1 y password 2 no concuerdan", "isError": True}
        try:
            new_user = User(
                password=request.json["password1"],
                email=request.json["email"],
            )
            db.session.add(new_user)
            db.session.commit()
            return {"mensaje": "usuario creado con exito", "isError": False}
        except exc.SQLAlchemyError:
            db.session.rollback()
            return {"mensaje": "Username o password invalidos", "isError": True}