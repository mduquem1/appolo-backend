from flask import Flask
from flask_restful import Api

from app.views import HealthCheck, Stores, SingUpView


def create_api(app: Flask) -> Api:
    api = Api(app)
    api.add_resource(HealthCheck, "/")
    api.add_resource(Stores, "/api/stores")
    api.add_resource(SingUpView, "/api/auth/signup")

    return api